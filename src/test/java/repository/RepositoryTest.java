package repository;

import com.devbugger.mongoplay.MongoPlay;
import com.devbugger.mongoplay.domain.Country;
import com.devbugger.mongoplay.domain.Person;
import com.devbugger.mongoplay.repository.CountryRepository;
import com.devbugger.mongoplay.repository.PersonRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

/**
 * @author Dag Østgulen Heradstveit
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = MongoPlay.class)
public class RepositoryTest {

    @Autowired
    private PersonRepository personRepository;

    @Autowired
    private CountryRepository countryRepository;

    @Before
    public void setUp() {
        personRepository.deleteAll();
        personRepository.save(new Person("Frank", "Exampleson"));
        personRepository.save(new Person("Johny", "Exampleson"));
        personRepository.save(new Person("Carry", "Exampleson"));
        personRepository.save(new Person("Alice", "Exampleson"));

        countryRepository.save(new Country("Norway", "NO"));

        personRepository.findAll().forEach(p -> {
            p.setCountry(countryRepository.findByName("Norway"));
            personRepository.save(p);
        });
    }

    @Test
    public void findAllPersonsTest() throws Exception {
        assertThat("Expected 4 entities. Found a different amount.",
                personRepository.findAll().size(), is(4)
        );
    }

    @Test
    public void findPersonByFirstNameTest() throws Exception {
        assertThat("Except Carry, did not get Carry.",
                personRepository.findByFirstName("Carry").getFirstName(), is("Carry")
        );
    }

    @Test
    public void findPersonsByLastNameTest() throws Exception {
        List<Person> persons = personRepository.findByLastName("Exampleson");

        assertThat("There are dead persons in the list!", persons.size(), is(4));
        persons.forEach((p) ->
                assertThat("There is an unknown person in the list!",
                        p.getLastName(), is("Exampleson"))
        );
    }

    @Test
    public void findPersonsByCountryNameTest() throws Exception {
        List<Person> persons = personRepository.findByCountry(countryRepository.findByName("Norway"));
        persons.forEach(p ->
                assertThat("Expected Norway. Did not get Norway.",
                        p.getCountry(), is(countryRepository.findByName("Norway")))
        );
    }

    @Test
    public void findCountryByCountryNameTest() throws Exception {
        assertThat("Expected Norway. Did not get Norway.",
                countryRepository.findByName("Norway"), is(countryRepository.findByName("Norway")));
    }
}