package com.devbugger.mongoplay;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.context.web.SpringBootServletInitializer;

/**
 * @author Dag Østgulen Heradstveit
 */
@EnableAutoConfiguration
public class MongoPlay extends SpringBootServletInitializer {

    public static void main(String[] args) {
        SpringApplication.run(MongoPlay.class, args);
    }
}