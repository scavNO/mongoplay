package com.devbugger.mongoplay.repository;

import com.devbugger.mongoplay.domain.Country;
import com.devbugger.mongoplay.domain.Person;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

/**
 * @author Dag Østgulen Heradstveit
 */
public interface PersonRepository extends MongoRepository<Person, String> {

    public Person findByFirstName(String firstName);
    public List<Person> findByLastName(String lastName);
    public List<Person> findByCountry(Country country);
}
