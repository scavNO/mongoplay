package com.devbugger.mongoplay.repository;

import com.devbugger.mongoplay.domain.Country;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * @author Dag Østgulen Heradstveit
 */
public interface CountryRepository  extends MongoRepository<Country, String> {

    public Country findByName(String name);
}
